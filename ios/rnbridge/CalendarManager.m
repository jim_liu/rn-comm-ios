#import "CalendarManager.h"
#import <React/RCTLog.h>

@implementation CalendarManager

RCT_EXPORT_MODULE();

RCT_EXPORT_METHOD(doNativeMethod: (int)a b:(int)b callback:(RCTResponseSenderBlock)callback)
{
  callback(@[[NSNull null], [NSNumber numberWithInt:a+b]]);
}

@end
