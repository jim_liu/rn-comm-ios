#import "NativeEventManager.h"

@implementation NativeEventManager
RCT_EXPORT_MODULE();

// 回傳支援的所有事件名稱
- (NSArray<NSString *> *)supportedEvents
{
  return @[@"MyNativeEvent"];
}

- (instancetype)init {
  // 監聽事件
  if (self = [super init]) {
    for (NSString *notifiName in [self supportedEvents]) {
      [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fireData:) name:notifiName object:nil];
    }
  }
  return self;
}

// 發送事件(到js)
int eventCnt = 99;
- (void)fireData:(NSNotification *)notification{
  //  if (notification)
  [self sendEventWithName:@"MyNativeEvent" body:@{@"eventCnt":@(eventCnt++), @"data":notification.userInfo}];
}

// 觸發事件
+(void)emit:(NSDictionary *)dictionary {
  [[NSNotificationCenter defaultCenter] postNotificationName:@"MyNativeEvent" object:nil userInfo:dictionary];
}

@end
