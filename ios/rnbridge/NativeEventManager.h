#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface NativeEventManager : RCTEventEmitter<RCTBridgeModule>

+ (void)emit:(NSDictionary *)dictionary;

@end
