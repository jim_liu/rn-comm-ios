#import <MapKit/MapKit.h>
#import <React/RCTViewManager.h>

#import "RNTMapView.h"
#import "RCTConvert+Mapkit.h"
#import "NativeEventManager.h"

@interface RNTMapManager : RCTViewManager <MKMapViewDelegate>
@end

@implementation RNTMapManager

RCT_EXPORT_MODULE()

RCT_EXPORT_VIEW_PROPERTY(zoomEnabled, BOOL)
RCT_EXPORT_VIEW_PROPERTY(onRegionChange, RCTBubblingEventBlock)

RCT_CUSTOM_VIEW_PROPERTY(region, MKCoordinateRegion, MKMapView)
{
  [view setRegion:json ? [RCTConvert MKCoordinateRegion:json] : defaultView.region animated:YES];
}

- (UIView *)view
{
  // 建立原生view
  RNTMapView *map = [RNTMapView new];
  map.delegate = self;
  return map;
}

#pragma mark MKMapViewDelegate

int clickCnt = 0;

- (void)mapView:(RNTMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
  if (!mapView.onRegionChange) {
    return;
  }
  MKCoordinateRegion region = mapView.region;
  
  NSDictionary *data = @{
                         @"region": @{
                             @"sn": @(clickCnt++),
                             @"latitude": @(region.center.latitude),
                             @"longitude": @(region.center.longitude),
                             @"latitudeDelta": @(region.span.latitudeDelta),
                             @"longitudeDelta": @(region.span.longitudeDelta),
                             }
                         };

  // 發送元件事件
  mapView.onRegionChange(data);
  
  // 發送原生事件
  [NativeEventManager emit:(data)];
}
@end
