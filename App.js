/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  Text,
} from 'react-native';

import { NativeEventEmitter, NativeModules } from 'react-native';
var CalendarManager = NativeModules.CalendarManager;

var NativeEventManager = NativeModules.NativeEventManager;
const nativeEventEmitter = new NativeEventEmitter(NativeEventManager);

import MapView from './MapView.js';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
    };

    // 叫用原生函數
    CalendarManager.doNativeMethod(2, 3, (error, result) => {
      if (error) {
        console.error(error);
      } else {
        this.setState({ nativeResult: result });
      }
    });

    // 註冊原生事件callback
    let subscription = nativeEventEmitter.addListener('MyNativeEvent', (event) => {
      this.setState({
        nativeEvent: event
      });
    });
  }

  //UI元件事件callback
  onRegionChange(event) {
    this.setState({
      uiEvent: event
    });

    // 從js發送事件
    // nativeEventEmitter.emit("MyNativeEvent", event);
  }

  render() {
    var region = {
      latitude: 37.48,
      longitude: -122.16,
      latitudeDelta: 0.1,
      longitudeDelta: 0.1,
    };
    return (
      <SafeAreaView>
        <MapView
          region={region}
          zoomEnabled={false}
          onRegionChange={this.onRegionChange.bind(this)}
          style={{ width: 100, height: 100 }}
        />
        <Text>
          native result : {this.state.nativeResult}
        </Text>
        <Text>
          UI Event : {JSON.stringify(this.state.uiEvent, null, "\t")}
        </Text>
        <Text>
          native event : {JSON.stringify(this.state.nativeEvent, null, "\t")}
        </Text>
      </SafeAreaView>
    );
  }
}

export default App;
