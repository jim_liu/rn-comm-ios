
```sh

# 安裝cli
npm install -g react-native-cli

# 建立project
react-native init rnbridge

```


```sh

# 編譯與執行ios app (會執行一個server)
# 每此ios程式碼有變更時都需要重新執行一次
yarn ios

# 關閉server
yarn kill

```

# 達成

![snapshot](./snapshot.png)

### js呼叫原生函數
> 參考CalendarManager

### 原生ui元件範例
> 參考RNTMap

### 原生事件發送
> 參考NativeEventManager

更多內容請參考官方文件
[官方文件](https://facebook.github.io/react-native/docs/native-modules-ios)

# 坑

> 從React v15.5開始，React.PropTypes助手函數已被棄用
```sh
npm install --save prop-types
```
```js
import PropTypes from 'prop-types';
```

> requireNativeComponent: `NativeComponent` was not found in the UIManager

> Tried to register two views with the same name `NativeComponent`

只要在requireNativeComponent的js做修改就會發生 server重啟才能恢復

> hot realod時原生元件的生命週期不會重來

> 建立原生模板套件react-native-create-bridge
對rn本版有依賴性 新版rn不建議使用

